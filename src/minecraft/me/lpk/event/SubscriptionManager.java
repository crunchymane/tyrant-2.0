package me.lpk.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

// TODO: Complete thread-safety
// TODO: If EventApple extends EventFruit, firing EventApple should invoke both EventApple methods and EventFruit ones.
/**
 * Event manager with efficient caching and priority-sorted event invocation.
 * 
 * @author GenericSkid
 * @since 6/18/17
 */
public class SubscriptionManager {
	/**
	 * Comparator used for sorting methods <i>(Wrapped)</i> by event subscription
	 * priority. Identity hashcode is appended to prevent same-priority events
	 * conflicting.
	 */
	private static final Comparator<MethodWrapper> METHOD_WRAPPER_COMPARE = Comparator
			.<MethodWrapper, Integer> comparing(SubscriptionManager::getSubscriptionPriority)
			.thenComparing(System::identityHashCode);
	/**
	 * Map of event types to their subscription handlers. Priority of events is
	 * per-method only. The declaring class of a method plays no role.
	 */
	private final Map<Class<? extends Event>, SubscriptionHandler> typeToSubscriptions = new ConcurrentHashMap<>();
	/**
	 * Map of classes to their declared methods that are viable for event
	 * subscription. Used as a cache so re-registering a class for events doesn't
	 * have to do all the same checks over again.
	 */
	private final Map<Class<?>, MethodCache> classToCache = new ConcurrentHashMap<>();
	/**
	 * Strategy for discovering subscribe annotations on methods.
	 */
	private AnnotationDiscoveryStrategy annoDiscovery;

	/**
	 * Register an object for event subscriptions. To subscribe a method to an
	 * event, apply a {@link me.lpk.event.Subscribe @Subscribe} annotation with the
	 * type of event to receive <i>(And optionally its priority relative to other
	 * methods receiving the same type. Higher values are invoked first)</i>. An
	 * example declaration would look like:
	 * 
	 * <pre>
	&#64;Subscribe( priority = 32)
	void myMethod(SomeEvent event) {
	    ...
	}
	 * </pre>
	 * 
	 * See {@link #unsubscribe(Object)} for unsubscribing an object from events.
	 * 
	 * @param instance
	 *            Object to subscribe to events.
	 */
	public void subscribe(Object instance) {
		MethodCache cache = isCached(instance) ? getCache(instance) : createCache(instance);
		for (Method method : cache.getMethods()) {
			Class<? extends Event> subscriptionType = cache.getSubscriptionType(method);
			getHandler(subscriptionType).register(cache.getWrapper(method));
		}
	}

	/**
	 * Unregister an object for event subscriptions. See {@link #subscribe(Object)}
	 * for subscribing an object to events.
	 * 
	 * @param instance
	 *            Object to unsubscribe events from.
	 */
	public void unsubscribe(Object instance) {
		MethodCache cache = isCached(instance) ? getCache(instance) : createCache(instance);
		for (Method method : cache.getMethods()) {
			Class<? extends Event> subscriptionType = cache.getSubscriptionType(method);
			getHandler(subscriptionType).unregister(cache.getWrapper(method));
		}
	}

	/**
	 * Sends an event to all objects subscribed to its type.
	 * 
	 * @param event
	 *            Event to send to subscriptions.
	 */
	public void invoke(Event event) {
		// TODO: Should support exceptions?
		//
		// IllegalArgumentException: Thrown if a subscribed method can not support the
		// type of event passed to it.
		//
		// ReflectiveOperationException: Thrown if an exception occurred invoking the
		// subscribed method.
		try {
			getHandler(event.getClass()).invoke(event);
		} catch (Exception e) {}
	}

	/**
	 * Create a SubscriptionHandler associated with the given event type.
	 * 
	 * @param subscriptionType
	 *            Event Type.
	 * @return SubscriptionHandler for event type.
	 */
	private SubscriptionHandler createHandler(Class<? extends Event> subscriptionType) {
		SubscriptionHandler handler = null;
		this.typeToSubscriptions.put(subscriptionType, handler = new SubscriptionHandler());
		return handler;
	}

	/**
	 * Retrieve the SubscriptionHandler associated with the given event type.
	 * 
	 * @param subscriptionType
	 *            Event Type.
	 * @return SubscriptionHandler for event type.
	 */
	private SubscriptionHandler getHandler(Class<? extends Event> subscriptionType) {
		return this.typeToSubscriptions.get(subscriptionType);
	}

	/**
	 * Check if the given subscription type has a handler.
	 * 
	 * @param subscriptionType
	 * @return
	 */
	private boolean isHandled(Class<? extends Event> subscriptionType) {
		return this.typeToSubscriptions.containsKey(subscriptionType);
	}

	/**
	 * Create a cache for methods in the given object instance.
	 * 
	 * @param instance
	 *            Object with methods to subscribe to events.
	 * @return MethodCache for the given object.
	 */
	private MethodCache createCache(Object instance) {
		Class<?> classKey = instance.getClass();
		// Create new cache and insert it into the map
		MethodCache cache = null;
		this.classToCache.put(classKey, cache = new MethodCache());
		// Populate cache with methods receiving events
		for (Method method : classKey.getDeclaredMethods()) {
			// Check if method has event-listener
			Class<? extends Event> subscriptionType = getSubscriptionType(method);
			if (subscriptionType != null && annoDiscovery.getAnnotation(method) != null) {
				// Create method wrapper
				cache.cacheMethod(method, instance, subscriptionType);
				// Create or get subscription handler for the event type
				if (!isHandled(subscriptionType)) {
					createHandler(subscriptionType);
				}
			}
		}
		return cache;
	}

	/**
	 * Retrieve the cache for methods in the given object instance.
	 * 
	 * @param instance
	 *            Object with methods to subscribe to events.
	 * @return MethodCache for the given object.
	 */
	private MethodCache getCache(Object instance) {
		return this.classToCache.get(instance.getClass());
	}

	/**
	 * Check if a given object has had its methods cached.
	 * 
	 * @param instance
	 *            Object with methods to subscribe to events.
	 * @return True if object has cached methods. False otherwise.
	 */
	private boolean isCached(Object instance) {
		return this.classToCache.containsKey(instance.getClass());
	}

	/**
	 * Ensures that any cached objects for the given instance are deleted.
	 * 
	 * @param instance
	 */
	public void destroyCache(Object instance) {
		Class<?> classKey = instance.getClass();
		if (this.classToCache.containsKey(classKey)) {
			this.classToCache.remove(classKey);
		}
	}

	/**
	 * Assuming a method has a {@link me.lpk.event.Subscribe} annotation, find the
	 * type of the {@link me.lpk.event.Subscribe#event() event type}.
	 * 
	 * @param method
	 *            Method with the annotation.
	 * @return Value of priority.
	 */
	@SuppressWarnings("unchecked")
	private Class<? extends Event> getSubscriptionType(Method method) {
		if (method.getParameterTypes().length != 1) {
			return null;
		}
		Class<?> paramType = method.getParameterTypes()[0];
		if (Event.class.isAssignableFrom(paramType)) {
			return (Class<? extends Event>) paramType;
		}
		return null;
	}

	/**
	 * Set the strategy for discovering subscription annotations on methods.
	 * 
	 * @param annoDiscovery
	 */
	public void setAnnotationStrategy(AnnotationDiscoveryStrategy annoDiscovery) {
		this.annoDiscovery = annoDiscovery;
	}

	/**
	 * Assuming a method <i>(which is contained by the wrapper)</i> has a
	 * {@link me.lpk.event.Subscribe} annotation, find the value of the
	 * {@link me.lpk.event.Subscribe#priority() priority}.
	 * 
	 * @param method
	 *            Warapped method with the annotation.
	 * @return Value of priority.
	 */
	private static int getSubscriptionPriority(MethodWrapper wrapper) {
		return wrapper.priority;
	}

	// -----------------------------------------------------------------------------

	/**
	 * Event listener indicator for methods.
	 * 
	 * @author GenericSkid
	 * @since 6/18/17
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Subscribe {
		/**
		 * Priority of the event subscription. Higher values are called before lower
		 * values.
		 * 
		 * @return
		 */
		public int priority() default 0;
	}

	// -----------------------------------------------------------------------------

	/**
	 * The basic event.
	 * 
	 * @author GenericSkid
	 * @since 6/18/17
	 */
	public static class Event {
		private boolean cancelled;

		public final void setCancelled(boolean cancelled) {
			this.cancelled = cancelled;
		}

		public final boolean isCancelled() {
			return this.cancelled;
		}
	}

	// -----------------------------------------------------------------------------

	/**
	 * Handler for which events should be invoked per event-type and in which order
	 * the methods should be invoked.
	 * 
	 * @author GenericSkid
	 * @since 6/18/17
	 */
	private class SubscriptionHandler {
		/**
		 * List that auto-sorts method wrappers on insertion.
		 */
		private final ConcurrentSkipListSet<MethodWrapper> subscribers = new ConcurrentSkipListSet<>(
				METHOD_WRAPPER_COMPARE);

		/**
		 * Register method for events.
		 * 
		 * @param method
		 */
		public void register(MethodWrapper method) {
			subscribers.add(method);
		}

		/**
		 * Unregister method for events
		 * 
		 * @param method
		 */
		public void unregister(MethodWrapper method) {
			subscribers.remove(method);
		}

		/**
		 * Passing events to registered methods.
		 * 
		 * @param event
		 */
		public void invoke(Event event) {
			for (MethodWrapper wrapper : subscribers) {
				wrapper.invoke(event);
			}
		}
	}

	// -----------------------------------------------------------------------------

	/**
	 * Cache of methods for a class.
	 * 
	 * @author GenericSkid
	 * @since 6/18/17
	 */
	private class MethodCache {
		private final Map<Method, MethodWrapper> methodToWrappers = new ConcurrentHashMap<>();
		private final Map<Method, Class<? extends Event>> methodToType = new ConcurrentHashMap<>();

		public MethodWrapper cacheMethod(Method method, Object instance, Class<? extends Event> eventType) {
			MethodWrapper wrapper;
			this.methodToType.put(method, eventType);
			this.methodToWrappers.putIfAbsent(method, wrapper = new MethodWrapper(method, instance));
			return wrapper;
		}

		public MethodWrapper getWrapper(Method method) {
			return methodToWrappers.get(method);
		}

		public Class<? extends Event> getSubscriptionType(Method method) {
			return this.methodToType.get(method);
		}

		public Set<Method> getMethods() {
			return this.methodToType.keySet();
		}
	}

	// -----------------------------------------------------------------------------

	/**
	 * Wrapper for quick reflection invocation of methods.
	 * 
	 * @author GenericSkid
	 * @since 6/18/17
	 */
	private class MethodWrapper implements Comparable<MethodWrapper> {
		private final Method method;
		private final Object instance;
		private final int priority;

		public MethodWrapper(Method method, Object instance) {
			this.method = method;
			this.instance = instance;
			this.priority = annoDiscovery.getAnnotation(method).priority();
		}

		public void invoke(Event event) {
			// TODO: There should be a better way to handle this.
			//
			// But lambdas literally won't compile when wrapped in try-catched
			// catching non-runtime exceptions. Should I just throw a
			// RuntimeException or a wrapper for the exception thrown that
			// extends RuntimeException?
			try {
				method.invoke(instance, event);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}

		@Override
		public int compareTo(MethodWrapper other) {
			return Integer.compare(priority, other.priority);
		}
	}

	// -----------------------------------------------------------------------------

	/**
	 * Strategy for retrieving subscription annotations from methods.
	 * 
	 * @author GenericSkid
	 * @since 6/18/17
	 */
	public interface AnnotationDiscoveryStrategy {
		/**
		 * Retrieves the subscribe annotation from the given method. Some methods may
		 * not have any.
		 * 
		 * @param method
		 * @return
		 */
		Subscribe getAnnotation(Method method);

		/**
		 * Basic strategy that checks for the annotation on the method declaration.
		 * 
		 * @author GenericSkid
		 * @since 6/18/17
		 */
		class Direct implements AnnotationDiscoveryStrategy {
			@Override
			public Subscribe getAnnotation(Method method) {
				return method.getDeclaredAnnotation(Subscribe.class);
			}
		}

		/**
		 * Strategy that checks for the annotation on the method's parent declaration
		 * <i>(where parents are the direct interfaces)</i>.
		 * 
		 * @author GenericSkid
		 * @since 6/18/17
		 */
		class Parent implements AnnotationDiscoveryStrategy {
			@Override
			public Subscribe getAnnotation(Method method) {
				return getParentDeclaration(method).getDeclaredAnnotation(Subscribe.class);
			}

			private static Method getParentDeclaration(Method method) {

				Class<?> c = method.getDeclaringClass();
				// check interfaces for method with exact matching descriptor
				for (Class<?> inter : c.getInterfaces()) {
					try {
						Method interfaceMethod = inter.getDeclaredMethod(method.getName(), method.getParameterTypes());
						if (interfaceMethod != null) {
							return getParentDeclaration(interfaceMethod);
						}
					} catch (NoSuchMethodException | SecurityException e) {}
				}
				// If none was found, return the given method as a fallback.
				return method;
			}
		}
	}
}