package me.lpk.tyrant.hook;

import java.io.IOException;

import javax.annotation.Nullable;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;

import me.lpk.tyrant.Tyrant;
import me.lpk.tyrant.gui.screen.InGameMenuHook;
import me.lpk.tyrant.gui.screen.MainMenuHook;
import me.lpk.tyrant.gui.screen.SetupMenu;
import me.lpk.tyrant.plugin.KeyHoldPlugin;
import me.lpk.tyrant.plugin.KeyTogglePlugin;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.main.GameConfiguration;

public class MinecraftHook extends Minecraft {
	private Tyrant tyrant;
	private boolean hasRunSetup = true;

	public MinecraftHook(GameConfiguration config) {
		super(config);
	}

	@Override
	protected void startGame() throws LWJGLException, IOException {
		super.startGame();
	}

	// ---------------------
	// ---- GAME EVENTS ----
	// ---------------------

	@Override
	public void displayGuiScreen(@Nullable GuiScreen screen) {
		// TODO: delegate this to events. Plugins can control this.
		if (screen instanceof GuiIngameMenu) {
			screen = new InGameMenuHook();
		} 
		if (screen instanceof GuiMainMenu) {
			if (tyrant == null) {
				tyrant = new Tyrant();
				tyrant.reload();
			}
			if (hasRunSetup) {
				screen = new MainMenuHook();
			} else {
				screen = new SetupMenu();
			}
		} 
		super.displayGuiScreen(screen);
	}

	// ---------------
	// ---- INPUT ----
	// ---------------

	@Override
	protected void clickMouse() {
		super.clickMouse();
	}

	@Override
	protected void rightClickMouse() {
		super.rightClickMouse();
	}

	@Override
	protected void middleClickMouse() {
		super.middleClickMouse();
	}

	@Override
	public void dispatchKeypresses() {
		int key = Keyboard.getEventKey() == 0 ? Keyboard.getEventCharacter() + 256 : Keyboard.getEventKey();
		if (key != 0 && !Keyboard.isRepeatEvent()) {
			boolean down = Keyboard.getEventKeyState();
			if (this.currentScreen == null) {
				handlePluginKeys(key, down);
			}
			super.dispatchKeypresses();
		}
	}

	private void handlePluginKeys(int key, boolean down) {
		// Handle toggle-able plugins.
		if (down) {
			for (KeyTogglePlugin plugin : tyrant.plugins.getToggles()) {
				// These plugins are toggled.
				if (key == plugin.getKey()) {
					plugin.toggle();
				}
			}
		}
		// Handle press-able plugins.
		for (KeyHoldPlugin plugin : tyrant.plugins.getPresses()) {
			// These plugins are active while their key's are pressed or
			// released. They can choose which action to activate on.
			if (key == plugin.getKey()) {
				if (down) {
					plugin.onKeyDown();
				} else {
					plugin.onKeyRelease();
				}
			}
		}

	}
}
