package me.lpk.tyrant.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.google.common.collect.Iterables;

import me.lpk.tyrant.util.exception.TyrantException;
import net.minecraft.client.resources.Locale;

public class Resources {
	private final File dir = new File("tyrant");
	private final File dirLang = new File(dir, "lang");
	private final File dirPlugin = new File(dir, "plugin");
	private final File dirCommand = new File(dir, "command");
	private final File dirConfig = new File(dir, "conf");
	private final Map<String, String> translations = new HashMap<>();
	private String language;

	public void reload() throws TyrantException {
		// Ensure tyrant directories exist
		ensureExistence();
		// Load translations
		loadLocale(language);
	}

	/**
	 * Returns the current language.
	 */
	public String getLanguage() {
		if (language == null) {
			return "en";
		}
		return language;
	}

	/**
	 * Sets the current language <i>(Reloads translation map)</i>
	 * 
	 * @param language
	 * @throws TyrantException
	 */
	public void setLanguage(String language) throws TyrantException {
		loadLocale(this.language = language);
	}

	/**
	 * Return the localized text for the given translation key.
	 * 
	 * @param key
	 * @return Localized text.
	 */
	public String translate(String key) {
		return this.translations.containsKey(key) ? this.translations.get(key) : key;
	}

	/**
	 * Populate the translations map with the given language's translations.
	 * 
	 * @param lang
	 *            Language to load. If null, value is defaulted to <i>en</i>.
	 * @throws TyrantException
	 *             Thrown if IOException is thrown.
	 */
	private void loadLocale(String lang) throws TyrantException {
		String langFileName = ((lang == null) ? "en" : lang) + ".lang";
		File translationsFile = new File(dirLang, langFileName);
		if (!translationsFile.exists()) {
			throw new TyrantException(
					"Translations file does not exist (Selected lang: " + lang + "): " + translationsFile.getName(),
					"The language file could not be found. Ensure the file name appears EXACTLY on your system as it does in this crash report. If it does not, then contact the developer.");
		}
		try {
			loadLocaleFromStream(new FileInputStream(translationsFile));
		} catch (FileNotFoundException e) {
			// We already checked for this.
		}
	}

	/**
	 * Populate the translations map from the given locale stream.
	 * 
	 * @param localeStream
	 *            Locale from steam.
	 * @throws TyrantException
	 *             Thrown if IOException occurred when reading from stream.
	 */
	private void loadLocaleFromStream(InputStream localeStream) throws TyrantException {
		try {
			for (String line : IOUtils.readLines(localeStream, StandardCharsets.UTF_8)) {
				if (!line.isEmpty() && line.charAt(0) != '#') {
					String[] lineSegments = (String[]) Iterables.toArray(Locale.SPLITTER.split(line), String.class);
					if (lineSegments != null && lineSegments.length == 2) {
						String key = lineSegments[0];
						String translation = Locale.PATTERN.matcher(lineSegments[1]).replaceAll("%$1s");
						translations.put(key, translation);
					}
				}
			}
		} catch (IOException e) {
			throw new TyrantException("Failed to read locale from steam: " + e.getMessage(),
					"An issue occured when reading from a language file. Does the file exist? Is it formatted properly?");
		}
	}

	/**
	 * Create required directories.
	 * 
	 * @throws TyrantException
	 */
	private void ensureExistence() throws TyrantException {
		// TODO: Automate creation of this array in case more directories exist
		// in the future.
		File[] directories = new File[] { dirLang, dirPlugin, dirCommand, dirConfig };
		for (File dir : directories) {
			if (!dir.exists()) {
				throw new TyrantException("Could not detect core directory: " + dir.getName(),
						"An important resource directory did not exist. Please ensure you have correctly installed Tryant. If you feel this is not an error on your behalf, contact the developer.");
			}
		}
	}

	/**
	 * Get or create a directory from the given path.
	 * 
	 * @param path
	 *            Path relative to the tyrant directory.
	 * @return
	 */
	public File getOrMakeDirectory(String path) {
		File folder = new File(dir, path);
		if (!folder.exists()) {
			folder.mkdirs();
		}
		return folder;
	}

	public File[] getPlugins() {
		return this.dirPlugin.listFiles();
	}
	
	public File[] getCommands() {
		return this.dirCommand.listFiles();
	}
}
