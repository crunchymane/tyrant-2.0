package me.lpk.tyrant.util.exception;

public class CommandPopulationException extends TyrantException {

	public CommandPopulationException(String message, String details) {
		super(message, details);
	}

}
