package me.lpk.tyrant.plugin.listener;

import me.lpk.event.SubscriptionManager.Event;
import me.lpk.event.SubscriptionManager.Subscribe;

public interface Tickable {
	@Subscribe
	void onTick(EventTick event);

	class EventTick extends Event {}
}
