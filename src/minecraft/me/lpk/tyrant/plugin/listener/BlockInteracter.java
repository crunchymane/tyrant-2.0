package me.lpk.tyrant.plugin.listener;

import me.lpk.event.SubscriptionManager.Event;
import me.lpk.event.SubscriptionManager.Subscribe;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public interface BlockInteracter {
	@Subscribe
	void onBlockBreak(EventBlockBreak event);

	@Subscribe
	void onBlockPlace(EventBlockPlace event);

	class EventBlockBreak extends Event {
		public final BlockPos pos;
		public final EnumFacing face;
		public final Block block;

		public EventBlockBreak(BlockPos pos, EnumFacing face, Block block) {
			this.pos = pos;
			this.face = face;
			this.block = block;
		}
	}

	class EventBlockPlace extends Event {
		public final BlockPos pos;
		public final EnumFacing face;
		public final ItemStack item;

		public EventBlockPlace(BlockPos pos, EnumFacing face, ItemStack item) {
			this.pos = pos;
			this.face = face;
			this.item = item;
		}
	}
}
