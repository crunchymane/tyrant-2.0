package me.lpk.tyrant.plugin;

import me.lpk.tyrant.Tyrant;

/**
 * Plugin that is toggled with a key-press.
 */
public abstract class KeyTogglePlugin extends KeyPlugin  {
	private boolean enabled;

	public KeyTogglePlugin(String name, String description, int key) {
		this(name, description, "plugin.type.toggle", key);
	}

	public KeyTogglePlugin(String name, String description, String category, int key) {
		super(name, description, category, key);
	}

	/**
	 * Toggles the status of the plugin. 
	 */
	public void toggle() {
		this.enabled = !this.enabled;
		if (this.enabled) {
			Tyrant.instance().events.subscribe(this);
			this.onEnable();
		} else {
			Tyrant.instance().events.unsubscribe(this);
			this.onDisable();
		}
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void onEnable() {}

	public void onDisable() {}
}
