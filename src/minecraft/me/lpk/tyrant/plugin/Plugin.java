package me.lpk.tyrant.plugin;

import me.lpk.tyrant.Tyrant;
import me.lpk.tyrant.plugin.util.MC;

/**
 * Basic plugin. Implementations need only give an onLoad / onUnload method.
 */
public abstract class Plugin implements MC {
	private final String name;
	private final String description;
	private final String category;
	private final PluginConfiguration configuration;

	public Plugin(String name, String description) {
		this(name, description, "plugin.type.default");
	}

	public Plugin(String name, String description, String category) {
		this.name = name;
		this.description = description;
		this.category = category;
		this.configuration = new PluginConfiguration(this);
	}

	/**
	 * Called when the plugin is loaded. Only called once per plugin-instance.
	 * 
	 * @param plugins
	 *            The plugin manager.
	 */
	public void onLoad(PluginManager plugins) {
		reloadConfig();
	}

	/**
	 * Called when the plugin is to be unloaded. Implementations should save
	 * per-plugin persistent values and close any resources being used.
	 * 
	 * @param plugins
	 *            The plugin manager.
	 */
	public void onUnload(PluginManager plugins) {
		configuration.onUnload();
	}

	/**
	 * Reloads values from the plugin's config file.
	 */
	public void reloadConfig() {
		configuration.onLoad();
	}

	/**
	 * The plugin identifier for back-end use.
	 * 
	 * @return
	 */
	public final String getID() {
		return name;
	}

	/**
	 * The translated name of the plugin. If no translations exist, the default name
	 * key is returned.
	 * 
	 * @return
	 */
	public final String getName() {
		return Tyrant.instance().resources.translate(name);
	}

	/**
	 * The translated description of the plugin. If no translations exist, the
	 * default description key is returned.
	 * 
	 * @return
	 */
	public final String getDescription() {
		return Tyrant.instance().resources.translate(description);
	}

	/**
	 * The translated type of the plugin. If no translations exist the default type
	 * key is returned. This should be used for categorizing plugins.
	 * 
	 * @return
	 */
	public final String getCategory() {
		return Tyrant.instance().resources.translate(category);
	}
}
