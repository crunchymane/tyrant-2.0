package me.lpk.tyrant.plugin.cli;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import me.lpk.tyrant.Tyrant;
import me.lpk.tyrant.util.Javac;
import me.lpk.tyrant.util.TLog;
import me.lpk.tyrant.util.exception.CommandExecutionException;
import me.lpk.tyrant.util.exception.CommandNotFound;
import me.lpk.tyrant.util.exception.CommandPopulationException;
import me.lpk.tyrant.util.exception.TyrantException;
import picocli.CommandLine;

public class CommandManager {
	private final Map<String, Command> commandMap = new HashMap<>();
	private final Javac<Command> compiler = new Javac<>();
	public String prefix = ".";

	public void executeCommand(String commandText)
			throws CommandNotFound, CommandExecutionException, CommandPopulationException {
		// Check if command is found
		String[] split = commandText.split(" ");
		String cmdKey = split[0];
		if (!commandMap.containsKey(cmdKey)) {
			throw new CommandNotFound("Command not found: '" + cmdKey + "'",
					"Perhaps you misspelled a command? Maybe the command isn't loaded?", commandText, cmdKey);
		}
		// If command is found, populate args and execute
		String[] args = new String[0];
		if (split.length > 0) {
			args = new String[split.length - 1];
			System.arraycopy(split, 1, args, 0, args.length);
		}
		Command command = commandMap.get(cmdKey);
		// Set up command via picocli
		try {
			CommandLine.populateCommand(command, args);
			// Execute command
			try {
				command.run();
			} catch (Exception e) {
				throw new CommandExecutionException("Failed to execute command: '" + command.getName() + "'",
						"Fail reason: " + e.getMessage());
			}
		} catch (Exception e) {
			throw new CommandPopulationException(
					"Failed to initialize command via Picocli: '" + command.getName() + "'",
					"Fail reason: " + e.getMessage());
		}
	}

	/**
	 * Add command.
	 * 
	 * @param command
	 */
	public void add(Command command) {
		TLog.info("Loaded command: " + command.getName());
		for (String alias : command.getAliases()) {
			commandMap.put(alias, command);
		}
	}

	/**
	 * Load commands.
	 */
	public void reload() {
		// Unload any existing plugins
		if (commandMap.size() > 0) {
			unload();
		}
		// Load commands
		for (File command : Tyrant.instance().resources.getCommands()) {
			if (command.getName().endsWith(".java")) {
				try {
					Class<Command> commandClass = compiler.compile(command);
					if (commandClass != null) {
						add(commandClass.newInstance());
					}
				} catch (IOException e) {
					TLog.warn(
							new TyrantException("Could not compile the class due to an IO error: " + command.getName(),
									"Fail reason: " + e.getMessage()));
				} catch (ClassCastException e) {
					TLog.warn(new TyrantException(
							"Could not cast the loaded class to Tyrant's Command class." + command.getName(),
							"The class should 'extends Command' and have the proper imports."));
				} catch (Exception e) {
					TLog.warn(new TyrantException("Exception thrown while loading command: " + command.getName(),
							"Reason: " + e.getMessage()));
				}
			}
		}
	}

	/**
	 * Commands don't have any data to save now, but maybe later custom <i>(User
	 * defined)</i> aliases and stuff could be added.
	 */
	public void unload() {
		commandMap.clear();

	}
	/**
	 * TODO: Having this public feels wrong. 
	 * @return
	 */
	public Map<String, Command> getCommandMap(){
		return commandMap;
	}
}
