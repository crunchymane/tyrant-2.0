package me.lpk.tyrant.plugin.util;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

public interface Rendering extends MC {
	static final float ANGLE_MAGIC = 0.017453292F;

	/**
	 * Mixes the values based on the given partial ticks.
	 * 
	 * @param partial
	 * @param prev
	 * @param cur
	 * @return
	 */
	default double mix(float partial, double prev, double cur) {
		return prev + (cur - prev) * partial;
	}

	/**
	 * Returns a mixed position for the given entity.
	 * 
	 * @param partial
	 * @param entity
	 * @return
	 */
	default Vec3d mix(float partial, Entity entity) {
		return new Vec3d(mix(partial, entity.prevPosX, entity.posX), mix(partial, entity.prevPosY, entity.posY),
				mix(partial, entity.prevPosZ, entity.posZ));
	}

	/**
	 * Returns a mixed position for the player's cursor.
	 * 
	 * @param partial
	 * @return
	 */
	default Vec3d getPlayerCursorPoint(float partial) {
		float yawCos = MathHelper.cos(-mc.player.rotationYaw * ANGLE_MAGIC - (float) Math.PI);
		float yawSin = MathHelper.sin(-mc.player.rotationYaw * ANGLE_MAGIC - (float) Math.PI);
		float pitchCos = -MathHelper.cos(-mc.player.rotationPitch * ANGLE_MAGIC);
		float pitchSin = MathHelper.sin(-mc.player.rotationPitch * ANGLE_MAGIC);
		Vec3d cursor = new Vec3d(yawSin * pitchCos, pitchSin, yawCos * pitchCos);
		cursor = cursor.add(mix(partial, mc.player));
		cursor = cursor.addVector(0, mc.player.getEyeHeight(), 0);
		return cursor;
	}

	/**
	 * Draws a box around the given position using RenderGlobal's drawBoundingBox.
	 * 
	 * @param pos
	 * @param red
	 * @param green
	 * @param blue
	 * @param alpha
	 */
	default void drawBlock(BlockPos pos, float red, float green, float blue, float alpha) {
		double mx = pos.getX();
		double my = pos.getY();
		double mz = pos.getZ();
		RenderGlobal.drawBoundingBox(mx, my, mz, mx + 1, my + 1, mz + 1, red, green, blue, alpha);
	}

	/**
	 * Fills a box given by the position with the assumption the box is a cube with
	 * a diameter of 1.
	 * 
	 * @param pos
	 * @param red
	 * @param green
	 * @param blue
	 * @param alpha
	 */
	default void fillBlock(BlockPos pos, float red, float green, float blue, float alpha) {
		double mx = pos.getX();
		double my = pos.getY();
		double mz = pos.getZ();
		RenderGlobal.renderFilledBox(mx, my, mz, mx + 1, my + 1, mz + 1, red, green, blue, alpha);
	}

	/**
	 * Draws a vertex;
	 * 
	 * @param v
	 */
	default void vertex3(Vec3d v) {
		vertex3(v.xCoord, v.yCoord, v.zCoord);
	}

	/**
	 * Draws a vertex.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	default void vertex3(double x, double y, double z) {
		GL11.glVertex3d(x, y, z);
	}

	/**
	 * Setup drawing lines.
	 * 
	 * @param setup
	 *            Is beginning of procedure.
	 */
	default void drawLines(boolean setup) {
		if (setup) {
			GL11.glBegin(GL11.GL_LINES);
		} else {
			GL11.glEnd();
		}
	}

	/**
	 * Setup drawing smooth lines.
	 * 
	 * @param setup
	 *            Is beginning of procedure.
	 */
	default void setSmoothLines(boolean setup) {
		if (setup) {
			GL11.glEnable(GL11.GL_LINE_SMOOTH);
		} else {
			GL11.glDisable(GL11.GL_LINE_SMOOTH);
		}
	}

	/**
	 * Enables and disables OpenGL caps based on if this is the beginning of a
	 * render procedure.
	 * 
	 * @param setup
	 *            Is beginning of procedure.
	 */
	default void standardRender(boolean setup) {
		if (setup) {
			GlStateManager.enableBlend();
			GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GlStateManager.glLineWidth(2f);
			GlStateManager.disableTexture2D();
			GlStateManager.enableCull();
			GlStateManager.disableDepth();
			GlStateManager.pushMatrix();
		} else {
			GlStateManager.popMatrix();
			GlStateManager.enableDepth();
			GlStateManager.enableTexture2D();
			GlStateManager.disableBlend();
		}
		translateRenderPos(setup);
	}

	/**
	 * Translates the render position.
	 * 
	 * @param setup
	 */
	default void translateRenderPos(boolean setup) {
		if (setup) {
			GL11.glTranslated(-mc.getRenderManager().renderPosX, -mc.getRenderManager().renderPosY,
					-mc.getRenderManager().renderPosZ);
		} else {
			GL11.glTranslated(mc.getRenderManager().renderPosX, mc.getRenderManager().renderPosY,
					mc.getRenderManager().renderPosZ);
		}
	}

}