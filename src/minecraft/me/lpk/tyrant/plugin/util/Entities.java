package me.lpk.tyrant.plugin.util;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.math.MathHelper;

// https://bitbucket.org/lordpankake/tyrant/src/1d1fa2448d04724deb7a6d70458b94d8a28a247d/src/minecraft/me/lordpankake/tyrant/util/EntityUtils.java?at=master&fileviewer=file-view-default
public interface Entities extends MC {

	/**
	 * Returns the angular distance from the mouse the entity is from the
	 * cursor.
	 * 
	 * @param entity
	 * @return Distance from cursor entity is.
	 */
	default float getDistanceFromMouse(Entity entity) {
		float[] neededRotations = getRotationsNeeded(entity);
		if (neededRotations != null) {
			float neededYaw = mc.player.rotationYaw - neededRotations[0];
			float neededPitch = mc.player.rotationPitch - neededRotations[1];
			float distanceFromMouse = MathHelper.sqrt((neededYaw * neededYaw) + (neededPitch * neededPitch));
			return distanceFromMouse;
		}
		return -1.0F;
	}

	/**
	 * Returns a float array with the yaw and pitch changes needed to look
	 * directly at the entity.
	 * 
	 * @param entity
	 * @return [deltaYaw, deltaPitch]
	 */
	default float[] getRotationsNeeded(Entity entity) {
		if (entity == null) {
			return null;
		}
		double diffX = entity.posX - mc.player.posX;
		double diffZ = entity.posZ - mc.player.posZ;
		double diffY;
		if (entity instanceof EntityLivingBase) {
			final EntityLivingBase elb = (EntityLivingBase) entity;
			diffY = (elb.posY + elb.getEyeHeight()) - (mc.player.posY + mc.player.getEyeHeight());
		} else {
			diffY = ((entity.boundingBox.minY + entity.boundingBox.maxY) / 2.0D)
					- (mc.player.posY + mc.player.getEyeHeight());
		}
		double dist = MathHelper.sqrt((diffX * diffX) + (diffZ * diffZ));
		float yaw = (float) ((Math.atan2(diffZ, diffX) * 180.0D) / 3.141592653589793D) - 90.0F;
		float pitch = (float) -((Math.atan2(diffY, dist) * 180.0D) / 3.141592653589793D);
		return new float[] { mc.player.rotationYaw + MathHelper.wrapDegrees(yaw - mc.player.rotationYaw),
				mc.player.rotationPitch + MathHelper.wrapDegrees(pitch - mc.player.rotationPitch) };
	}
}
