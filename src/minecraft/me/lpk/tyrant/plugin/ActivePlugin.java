package me.lpk.tyrant.plugin;

import me.lpk.tyrant.Tyrant;

/**
 * Plugin that receives events constantly. It can not be toggled.
 */
public class ActivePlugin extends Plugin {

	public ActivePlugin(String name, String description) {
		this(name, description, "plugin.type.active");
	}

	public ActivePlugin(String name, String description, String category) {
		super(name, description, category);
	}

	@Override
	public void onLoad(PluginManager plugins) {
		Tyrant.instance().events.subscribe(this);
	}

	@Override
	public void onUnload(PluginManager plugins) {
		Tyrant.instance().events.unsubscribe(this);
	}
}
