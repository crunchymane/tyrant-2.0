package me.lpk.tyrant.plugin;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import me.lpk.event.SubscriptionManager;
import me.lpk.tyrant.Tyrant;
import me.lpk.tyrant.util.TLog;
import me.lpk.tyrant.util.exception.TyrantException;
import me.lpk.tyrant.util.Javac;

public class PluginManager {
	private final Set<String> categories = new HashSet<>();
	private final Map<String, Plugin> idLookup = new HashMap<>();
	private final Set<KeyTogglePlugin> toggles = new HashSet<>();
	private final Set<KeyHoldPlugin> presses = new HashSet<>();
	private final Set<KeyPlugin> keys = new HashSet<>();
	private final Javac<Plugin> compiler = new Javac<>();

	public void add(Plugin plugin) {
		idLookup.put(plugin.getID(), plugin);
		TLog.info("Loaded plugin: " + plugin.getName());
		plugin.onLoad(this);
		// Add to set of plugin types
		if (!categories.contains(plugin.getCategory())) {
			categories.add(plugin.getCategory());
		}
		// Add to typed-plugin sets for ease of access.
		if (plugin instanceof KeyTogglePlugin) {
			toggles.add((KeyTogglePlugin) plugin);
			keys.add((KeyPlugin) plugin);
		}
		if (plugin instanceof KeyHoldPlugin) {
			presses.add((KeyHoldPlugin) plugin);
			keys.add((KeyPlugin) plugin);
		}
	}

	/**
	 * Load plugins.
	 */
	public void reload() {
		// Unload any existing plugins
		if (idLookup.size() > 0) {
			unload();
		}
		// Load plugins
		for (File plugin : Tyrant.instance().resources.getPlugins()) {
			if (plugin.getName().endsWith(".java")) {
				try {
					Class<Plugin> pluginClass = compiler.compile(plugin);
					if (pluginClass != null) {
						add(pluginClass.newInstance());
					}
				} catch (IOException e) {
					TLog.warn(new TyrantException("Could not compile the class due to an IO error: " + plugin.getName(),
							"Fail reason: " + e.getMessage()));
				} catch (ClassCastException e) {
					TLog.warn(new TyrantException(
							"Could not cast the loaded class to Tyrant's plugin class." + plugin.getName(),
							"The class should extend one of the given plugin classes and have the proper imports."));
				} catch (Exception e) {
					TLog.warn(new TyrantException("Exception thrown while loading plugin: " + plugin.getName(),
							"Reason: " + e.getMessage()));
				}
			}
		}
	}

	/**
	 * Ensure plugins save any data they need to before killing the manager.
	 */
	public void unload() {
		SubscriptionManager subs = Tyrant.instance().events;
		for (Plugin plugin : idLookup.values()) {
			// Clear event subscribptions
			subs.unsubscribe(plugin);
			subs.destroyCache(plugin);
			// Unload plugin
			plugin.onUnload(this);
			TLog.info("Unloaded plugin: " + plugin.getName());
		}
		// Clear plugins
		categories.clear();
		idLookup.clear();
		presses.clear();
		keys.clear();
		toggles.clear();
	}

	/**
	 * Retrieves the plugin by its ID.
	 * 
	 * @param id
	 * @return
	 */
	public Plugin getPlugin(String id) {
		return idLookup.get(id);
	}

	/**
	 * Retrieves the plugin by its translated name.
	 * 
	 * @param name
	 * @return
	 */
	public Plugin getPluginByName(String name) {
		for (Plugin plugin : idLookup.values()) {
			if (plugin.getName().equals(name)) {
				return plugin;
			}
		}
		return null;
	}

	/**
	 * Returns the collection of all plugins.
	 * 
	 * @return
	 */
	public Collection<Plugin> getPlugins() {
		return idLookup.values();
	}

	/**
	 * Returns the set of plugin categories.
	 * 
	 * @return
	 */
	public Set<String> getCategoryNames() {
		return categories;
	}

	/**
	 * Returns the set of plugins that are toggled by a key press.
	 * 
	 * @return
	 */
	public Set<KeyTogglePlugin> getToggles() {
		return toggles;
	}

	/**
	 * Returns the set of plugins that are active while a key is down, or the
	 * inverse.
	 * 
	 * @return
	 */
	public Set<KeyHoldPlugin> getPresses() {
		return presses;
	}

	/**
	 * Returns the set of plugins that have keybinds. See {@link #getPresses()} for
	 * active-while-down <i>(or the inverse)</i> plugins, and {@link #getToggles()}
	 * for toggleable keybound plugins.
	 * 
	 * @return
	 */
	public Set<KeyPlugin> getKeyPlugins() {
		return keys;
	}
}
