package me.lpk.tyrant.gui.theme;


import me.lpk.tyrant.gui.UIScreen;
import me.lpk.tyrant.gui.component.Component;

public abstract class Theme {
	public static final Theme BASIC_THEME = new BasicTheme();

	public abstract void onRender(UIScreen screen, Component component);
}
