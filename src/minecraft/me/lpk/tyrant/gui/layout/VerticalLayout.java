package me.lpk.tyrant.gui.layout;

import me.lpk.tyrant.gui.component.Component;
import me.lpk.tyrant.gui.component.Size;
import me.lpk.tyrant.gui.component.Component.Direction;
import me.lpk.tyrant.gui.component.Container;
import me.lpk.tyrant.gui.component.type.Layout;
import me.lpk.tyrant.gui.component.type.LayoutContainer;

public class VerticalLayout implements Layout {
	private final Container parent;

	public VerticalLayout(Container parent) {
		this.parent = parent;
	}

	@Override
	public void invalidate() {
		Component last = null;
		for (Component child : parent.getChildren()) {
			child.setSize(child.getMinimumSize());
			child.setX(parent.getPadding(Direction.WEST) + child.getMargin(Direction.WEST));
			if (last == null) {
				child.setY(child.getMargin(Direction.NORTH) + parent.getPadding(Direction.NORTH));
			} else {
				child.setY(child.getMargin(Direction.NORTH) + +last.getMargin(Direction.SOUTH) + last.getY()
						+ last.getDisplayHeight() + 1);
			}
			last = child;
			child.setWidth(parent.getWidth() - parent.getPadding(Direction.EAST) - parent.getPadding(Direction.WEST));
		}
	}

	@Override
	public Size getPackedSize(LayoutContainer owner) {
		int packedWidth = 0;
		int packedHeight = 0;
		for (Component child : owner.getChildren()) {
			int compSizeX = child.getX() + child.getDisplayWidth() + getHMargin(child);
			int compSizeY = child.getY() + child.getDisplayHeight() + getVMargin(child);
			if (compSizeX > packedWidth) {
				packedWidth = compSizeX;
			}
			if (compSizeY > packedHeight) {
				packedHeight = compSizeY;
			}
		}
		int pEast = owner.getContainer().getPadding(Direction.EAST);
		int pSouth = owner.getContainer().getPadding(Direction.SOUTH);
		int mEast = owner.getContainer().getMargin(Direction.EAST);
		int mSouth = owner.getContainer().getMargin(Direction.SOUTH);
		mEast = mSouth = 0;
		return new Size(packedWidth - pEast - mEast, packedHeight - pSouth - mSouth);
	}

	private int getHMargin(Component child) {
		return child.getMargin(Direction.EAST);
	}

	private int getVMargin(Component child) {
		return child.getMargin(Direction.SOUTH);
	}

}
