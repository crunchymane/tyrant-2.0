package me.lpk.tyrant.gui.layout;

import me.lpk.tyrant.gui.component.Component;
import me.lpk.tyrant.gui.component.Size;
import me.lpk.tyrant.gui.component.Component.Direction;
import me.lpk.tyrant.gui.component.impl.Spacer;
import me.lpk.tyrant.gui.component.Container;
import me.lpk.tyrant.gui.component.type.Layout;
import me.lpk.tyrant.gui.component.type.LayoutContainer;

public class BorderLayout implements Layout {
	private final Container parent;
	private Component lastSouth;

	public BorderLayout(Container parent) {
		this.parent = parent;
	}

	@Override
	public void invalidate() {
		// Components
		Component north = get(Direction.NORTH);
		Component south = get(Direction.SOUTH);
		Component west = get(Direction.WEST);
		Component east = get(Direction.EAST);
		Component center = get(Direction.CENTER);
		// Component groups
		Component[] mid = new Component[] { west, center, east };
		Component[] poles = new Component[] { north, south };
		// Setup north position
		north.setX(north.getMargin(Direction.WEST) + parent.getPadding(Direction.WEST));
		north.setY(north.getMargin(Direction.NORTH) + parent.getPadding(Direction.NORTH));
		// Get group sizes (for actual and display)
		Size midSize = getSize(mid);
		Size midSizeDisp = getDisplaySize(mid);
		Size poleSize = getPoleSize(midSizeDisp, poles);
		// Find the largest southern margin from the middle-group so it can be used
		// later.
		int southMargin = 0;
		for (Component c : mid) {
			// Set same y-pos and height for all
			c.setHeight(midSize.height);
			c.setY(north.getY() + north.getDisplayHeight() + north.getMargin(Direction.SOUTH)
					+ c.getMargin(Direction.NORTH));
			// margin check
			int sm = c.getMargin(Direction.SOUTH);
			if (sm > southMargin) {
				southMargin = sm;
			}
		}
		// Setting up middle group positions
		west.setX(west.getMargin(Direction.WEST) + parent.getPadding(Direction.WEST));
		center.setX(west.getX() + west.getDisplayWidth() + west.getMargin(Direction.EAST)
				+ center.getMargin(Direction.WEST));
		east.setX(center.getX() + center.getDisplayWidth() + center.getMargin(Direction.EAST)
				+ east.getMargin(Direction.WEST));
		// Setup southern position, based on middle group
		south.setX(west.getX());
		south.setY(west.getY() + midSizeDisp.height + south.getMargin(Direction.NORTH) + southMargin);
		// Correct pole's widths
		south.setWidth(poleSize.width - south.getPadding(Direction.EAST) - south.getPadding(Direction.WEST));
		north.setWidth(poleSize.width - north.getPadding(Direction.EAST) - north.getPadding(Direction.WEST));
		// Update last south so #getPackedSize can use it for easy calculation of width.
		lastSouth = south;
	}

	@Override
	public Size getPackedSize(LayoutContainer owner) {
		int packedWidth = lastSouth.getX() + lastSouth.getDisplayWidth() + getHMargin(lastSouth);
		int packedHeight = lastSouth.getY() + lastSouth.getDisplayHeight() + getVMargin(lastSouth);
		int pEast = owner.getContainer().getPadding(Direction.EAST);
		int pSouth = owner.getContainer().getPadding(Direction.SOUTH);
		int mEast = owner.getContainer().getMargin(Direction.EAST);
		int mSouth = owner.getContainer().getMargin(Direction.SOUTH);
		return new Size(packedWidth - pEast - mEast, packedHeight - pSouth - mSouth);
	}

	/**
	 * Create a size that can contain the given components placed horizontally next
	 * to one another.
	 * 
	 * @param comps
	 *            Array of components.
	 * @return Size to contain all.
	 */
	private Size getSize(Component... comps) {
		int w = 0, h = 0;
		for (Component c : comps) {
			c.setSize(c.getMinimumSize());
			w += c.getWidth();
			if (h < c.getHeight()) {
				h = c.getHeight();
			}
		}
		return new Size(w, h);
	}

	/**
	 * Create a size that can contain the given components placed horizontally next
	 * to one another.
	 * 
	 * @param comps
	 *            Array of components.
	 * @return Size to contain all.
	 */
	private Size getDisplaySize(Component... comps) {
		int w = 0, h = 0;
		for (Component c : comps) {
			c.setSize(c.getMinimumSize());
			w += c.getDisplayWidth();
			if (h < c.getDisplayHeight()) {
				h = c.getDisplayHeight();
			}
		}
		return new Size(w, h);
	}

	/**
	 * Calculate the correct size of north/south elements given the middle's size.
	 * 
	 * @param middle
	 *            Size of middle components.
	 * @param poles
	 *            Pole components.
	 * @return Size for each pole to fill.
	 */
	private Size getPoleSize(Size middle, Component[] poles) {
		Size max = new Size(0, 0);
		for (Component c : poles) {
			if (c.getDisplayWidth() > max.width) {
				max.width = c.getDisplayWidth();
			}
			if (c.getHeight() > max.height) {
				max.height = c.getHeight();
			}
		}
		if (middle.width > max.width) {
			max.width = middle.width;
		}
		return max;
	}

	/**
	 * Get component associated with given direction, return a spacer <i>(empty
	 * component)</i> if no association exists.
	 * 
	 * @param key
	 *            Direction.
	 * @return Component for direction.
	 */
	private Component get(Direction key) {
		return parent.getPropertyOrDefault(key.name(), new Spacer(0, 0, 0, 0));
	}

	private int getHMargin(Component child) {
		return child.getMargin(Direction.EAST);
	}

	private int getVMargin(Component child) {
		return child.getMargin(Direction.SOUTH);
	}

}
