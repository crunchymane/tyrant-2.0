package me.lpk.tyrant.gui;

import me.lpk.tyrant.gui.theme.Theme;
import net.minecraft.client.gui.FontRenderer;

public interface UIScreen {
	FontRenderer getFontRenderer();
		
	Theme getTheme();

	void setTheme(Theme theme);
}
