package me.lpk.tyrant.gui.screen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.lpk.tyrant.Tyrant;
import me.lpk.tyrant.gui.UIScreen;
import me.lpk.tyrant.gui.component.Frame;
import me.lpk.tyrant.gui.component.TextComponent;
import me.lpk.tyrant.gui.component.ClickComponent;
import me.lpk.tyrant.gui.component.Component;
import me.lpk.tyrant.gui.component.Component.Direction;
import me.lpk.tyrant.gui.component.Container;
import me.lpk.tyrant.gui.component.impl.PluginToggleButton;
import me.lpk.tyrant.gui.layout.BorderLayout;
import me.lpk.tyrant.gui.layout.VerticalLayout;
import me.lpk.tyrant.gui.theme.Theme;
import me.lpk.tyrant.plugin.KeyTogglePlugin;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;

public class ClickGuiScreen extends GuiScreen implements UIScreen {
	private final List<Frame> frames = new ArrayList<>();
	private final List<Component> components = new ArrayList<>();
	private Frame lastSelected = null;
	private int lastX, lastY;
	private Theme theme = Theme.BASIC_THEME;

	public ClickGuiScreen() {
		// setupFrames();
	}

	@Override
	public void initGui() {
		try {
			if (!(frames.isEmpty() && components.isEmpty())) {
				return;
			}
			addFrames();
			addComponents();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addComponents() {
		
	}

	private void addFrames() {
		int offsetX = 90;
		int offsetY = 10;
		int dragHeight = 20;
		int minWidth = 85;
		Map<String, Frame> typeToFrame = new HashMap<>();
		Set<String> pluginCategories = Tyrant.instance().plugins.getCategoryNames();
		// Create frames
		for (String category : pluginCategories) {
			// Creating the frame
			Frame frame = new Frame(offsetX, offsetY, dragHeight);
			frame.setWidth(minWidth);
			frame.setLayout(new VerticalLayout(frame));
			// Frame title
			TextComponent title = new TextComponent(category);
			title.setMargin(Direction.SOUTH, 4);
			title.setProperty("title", true);
			frame.add(title);
			//
			typeToFrame.put(category, frame);
			offsetX += minWidth;
		}
		// Add Mod buttons
		for (KeyTogglePlugin plugin : Tyrant.instance().plugins.getToggles()) {
			Frame frame = typeToFrame.get(plugin.getCategory());
			PluginToggleButton button = new PluginToggleButton(plugin);
			frame.add(button);
			// This frame now has content, so we should add it to the sceen.
			if (!frames.contains(frame)) {
				frames.add(frame);
			}
		}

		// Test frame
		{
			Frame test = new Frame(53, 33, 20);
			test.setPadding(3);
			test.setLayout(new BorderLayout(test));
			TextComponent title = new TextComponent("BorderLayout");
			title.setProperty("title", true);
			title.setMargin(Direction.SOUTH, 5);
			title.setMargin(Direction.EAST, 19);
			test.add(title, Direction.NORTH);
			Container center = new Container(0,0,2,2);
			center.setLayout(new BorderLayout(center));
			{
				center.setPadding(3);
				center.add(new TextComponent("North"), Direction.NORTH);
				//center.add(new TextComponent("South"), Direction.SOUTH);
				center.add(new TextComponent("East"), Direction.EAST);
				//center.add(new TextComponent("West"), Direction.WEST);
				//center.add(new TextComponent("Center"), Direction.CENTER);
			}
			test.add(center, Direction.CENTER);
			
			/*	
		    Component center = frame.getProperty(Direction.CENTER.name());
			center.setMargin(Direction.SOUTH, 3);
			frame.getLayout().invalidate();
			*/
			frames.add(test);
		}
	}

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int button) throws IOException {
		updateLastPosition(mouseX, mouseY);
		// Handle frame interactions
		Frame selected = null;
		boolean dragging = false;
		for (Frame frame : frames) {
			if (frame.isMouseOver(mouseX, mouseY)) {
				selected = frame;
				if (frame.isInDragRegion(mouseX, mouseY)) {
					dragging = true;
				} else {
					selected.onClick(mouseX, mouseY, button);
				}
				break;
			}
		}
		if (selected != null) {
			lastSelected = selected;
			if (dragging) {
				// Setup dragging
				lastSelected.setIsDragged(true);
			}
			// Move frame to first index of list (top)
			frames.remove(lastSelected);
			frames.add(0, lastSelected);
			return;
		}
		// Handle non-frame interactions
		for (Component component : components) {
			if (component.isMouseOver(mouseX, mouseY)) {
				component.onClick(mouseX, mouseY, button);
			}
		}
	}

	@Override
	protected void mouseReleased(int mouseX, int mouseY, int button) {
		updateLastPosition(mouseX, mouseY);
		if (lastSelected != null) {
			lastSelected.setIsDragged(false);
			lastSelected = null;
			// mc.displayGuiScreen(new ClickGuiScreen());
		}
	}

	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int button, long timeSinceLastClick) {
		if (lastSelected != null) {
			int dx = mouseX - lastX;
			int dy = mouseY - lastY;
			lastSelected.onDrag(dx, dy);
		}
		updateLastPosition(mouseX, mouseY);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		// hack to smooth render
		try {
			handleInput();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Render items in opposite order so top-most items are drawn last.
		for (int i = components.size() - 1; i >= 0; i--) {
			components.get(i).onRender(this, theme);
		}
		for (int i = frames.size() - 1; i >= 0; i--) {
			frames.get(i).onRender(this, theme);
		}
	}

	private void updateLastPosition(int mouseX, int mouseY) {
		lastX = mouseX;
		lastY = mouseY;
	}

	@Override
	public FontRenderer getFontRenderer() {
		return this.fontRendererObj;
	}

	@Override
	public Theme getTheme() {
		return theme;
	}

	@Override
	public void setTheme(Theme theme) {
		this.theme = theme;
	}

}
