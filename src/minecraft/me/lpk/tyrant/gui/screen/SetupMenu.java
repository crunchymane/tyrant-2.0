package me.lpk.tyrant.gui.screen;

import java.io.IOException;

import me.lpk.tyrant.util.Colors;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.text.TextFormatting;

public class SetupMenu extends PanoramaMenu {
	@Override
	public void initGui() {
		super.initGui();
		int endX = width / 3;
		int yStart = height - 30;
		this.buttonList.add(new GuiButton(0, 10, yStart, endX - (10 * 2), 20, "Skip Tutorial"));
	}

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int button) throws IOException {
		mc.displayGuiScreen(new SetupMenu());
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		int baseColor = Colors.getColor(0, 0, 0, 100);
		int falloff = 20;
		int startX = -1;
		int endX = width / 3;
		int y = 10;
		float scale = endX / 60f;

		super.drawScreen(mouseX, mouseY, partialTicks);
		
		this.drawScaledCenteredString(scale, getFontRenderer(), TextFormatting.BOLD + "" + TextFormatting.UNDERLINE +  "Tyrant", endX / 2, y, -1);
		this.drawScaledCenteredString(scale / 2f, getFontRenderer(), TextFormatting.UNDERLINE +  "Minecraft 1.12", endX / 2, (int) (y + getFontRenderer().FONT_HEIGHT * (1.24f * scale)), Colors.getColor(200, 200, 200));
	}
}
