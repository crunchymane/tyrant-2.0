package me.lpk.tyrant.gui.screen;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import me.lpk.tyrant.Tyrant;
import me.lpk.tyrant.plugin.cli.Command;
import me.lpk.tyrant.util.Colors;
import me.lpk.tyrant.util.TLog;
import me.lpk.tyrant.util.exception.CommandExecutionException;
import me.lpk.tyrant.util.exception.CommandNotFound;
import me.lpk.tyrant.util.exception.CommandPopulationException;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.GuiUtilRenderComponents;
import net.minecraft.util.TabCompleter;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

public class ConsoleScreen extends GuiScreen {
	/**
	 * The text field for input.
	 */
	protected GuiTextField inputField;
	/**
	 * Tab completion.
	 */
	private TabCompleter tabCompleter;
	/**
	 * List of messages shown on the console output.
	 */
	private List<ITextComponent> messages = new ArrayList<>();
	/**
	 * List of strings that were entered as input.
	 */
	private List<String> historyInputs = new ArrayList<>();
	/**
	 * Scroll position.
	 */
	private int scrollIndex;
	/**
	 * Number of lines allowed to be rendered.
	 */
	private int lineCount = 21;
	/**
	 * Index used for keeping track of history selection.
	 */
	private int historyIndex = -1;
	/**
	 * Buffer for history selections.
	 */
	private String historyBuffer = "";
	/**
	 * If true, content being printed to console should be done so by plugins.
	 * Otherwise, content is being printed via the console class.
	 */
	private boolean printIndexLock;
	/**
	 * The number of lines to update in the case {@link #updateLastSent(String)} is
	 * called.
	 */
	private int unlockedCount;
	/**
	 * The number of lines added to console since {@link #printIndexLock} was set to
	 * true.
	 */
	private int lockedMessagCount;

	@Override
	public void initGui() {
		// Enable repeat keys so you can make "re" into "reeeeeeeeeeeeeeeeeeee"
		Keyboard.enableRepeatEvents(true);
		inputField = new GuiTextField(0, this.fontRendererObj, 4, this.height - 12, this.width - 4, 12);
		inputField.setMaxStringLength(512);
		inputField.setEnableBackgroundDrawing(false);
		inputField.setFocused(true);
		inputField.setCanLoseFocus(false);
		tabCompleter = new ConsoleTabCompleter(this.inputField);
	}

	@Override
	public void onGuiClosed() {
		// Disable repeat events so keybinds don't get all fucky
		Keyboard.enableRepeatEvents(false);
	}

	@Override
	public void updateScreen() {
		// Update textfield cursor blink
		inputField.updateCursorCounter();
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException {
		tabCompleter.resetRequested();
		// Tab completion
		if (keyCode == Keyboard.KEY_TAB) {
			// Request tab option
			tabCompleter.complete();
		} else {
			// Reset
			tabCompleter.resetDidComplete();
			tabCompleter.getCompletions().clear();
		}
		if (keyCode == Keyboard.KEY_ESCAPE) {
			// Close screen
			mc.displayGuiScreen((GuiScreen) null);
		} else if (keyCode == Keyboard.KEY_RETURN || keyCode == Keyboard.KEY_NUMPADENTER) {
			// TODO: Parse content
			String content = inputField.getText().trim();
			if (!content.isEmpty()) {
				// Insert content into history
				historyInputs.add(0, content);
				// Execute command and add to message display
				execute(content);
				// clear
				inputField.setText("");
				// Update history index
				historyIndex = -1;
			}
		} else {
			if (keyCode == Keyboard.KEY_UP) {
				getSentHistory(1);
			} else if (keyCode == Keyboard.KEY_DOWN) {
				getSentHistory(-1);
			} else if (keyCode == Keyboard.KEY_PRIOR) {
				scroll(lineCount - 1);
			} else if (keyCode == Keyboard.KEY_NEXT) {
				scroll(-lineCount + 1);
			} else {
				inputField.textboxKeyTyped(typedChar, keyCode);
			}
		}

	}

	@Override
	public void handleMouseInput() throws IOException {
		super.handleMouseInput();
		// History scrolling
		int amount = Mouse.getEventDWheel();
		if (amount != 0) {
			if (amount > 1) {
				amount = 1;
			} else if (amount < -1) {
				amount = -1;
			}
			// Shift for slow scroll
			if (!isShiftKeyDown()) {
				amount *= 7;
			}
			// Update scroll
			scroll(amount);
		}
	}

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		this.inputField.mouseClicked(mouseX, mouseY, mouseButton);
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}

	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		int black = Colors.getColor(0, 0, 0);
		int blackOpac = Integer.MIN_VALUE;
		// Draw chat input
		drawRect(2, this.height - 14, this.width - 2, this.height - 2, blackOpac);
		inputField.drawTextBox();

		// Draw messages
		int endY = height - 20;
		int lineSize = 10;
		drawRect(2, endY - (lineSize * lineCount), this.width - 2, endY, blackOpac);
		int height = endY;
		for (int line = scrollIndex; line < lineCount + scrollIndex; line++) {
			if (line >= this.messages.size())
				break;
			ITextComponent message = this.messages.get(line);
			drawString(fontRendererObj, message.getFormattedText(), 5, height - lineSize, -1);
			height -= lineSize;
		}

		// Draw suggestions
		height = endY;
		int endIndex = inputField.getText().lastIndexOf(" ");
		if (endIndex < 0) {
			endIndex = inputField.getText().length();
		}
		int startX = fontRendererObj.getStringWidth(inputField.getText().substring(0, endIndex));
		int endX = startX + 10;
		// Calculate box dimensions needed to fill suggestions
		for (String completion : tabCompleter.getCompletions()) {
			int tmpEndX = startX + fontRendererObj.getStringWidth(completion) + 4;
			if (tmpEndX > endX) {
				endX = tmpEndX;
			}
		}
		// Draw suggestions + selected overlay
		int complIndex = tabCompleter.getCompletionIndex();
		int compls = tabCompleter.getCompletions().size();
		if (compls > 0) {
			drawRect(startX, endY - (compls * lineSize), endX, endY, black);
			for (int i = compls - 1; i >= 0; i--) {
				String completion = tabCompleter.getCompletions().get(i);
				// Highlight current suggestion selection
				if (i == complIndex - 1) {
					completion = TextFormatting.GOLD + completion;
				}
				drawString(fontRendererObj, completion, startX + 2, height - lineSize, -1);
				height -= lineSize;
			}
		}
		// Default
		super.drawScreen(mouseX, mouseY, partialTicks);
	}

	/**
	 * Retrieve sent messages.
	 * 
	 * @param direction
	 *            -1 for older messages, 1 for newer messages.
	 */
	private void getSentHistory(int direction) {
		int index = historyIndex + direction;
		int sentLength = historyInputs.size();
		// Prevent index OOB
		index = MathHelper.clamp(index, 0, sentLength);
		// Update if index has been changed
		if (index != historyIndex) {
			if (index == sentLength) {
				historyIndex = sentLength;
				inputField.setText(this.historyBuffer);
			} else {
				if (historyIndex == sentLength) {
					historyBuffer = inputField.getText();
				}
				inputField.setText(historyInputs.get(index));
				historyIndex = index;
			}
		}
	}

	private void scroll(int amount) {
		if (amount > 0) {
			amount = 1;
		} else {
			amount = -1;
		}
		this.scrollIndex += amount;
		int i = this.messages.size();
		if (this.scrollIndex > i - lineCount) {
			this.scrollIndex = i - lineCount;
		}
		if (this.scrollIndex <= 0) {
			this.scrollIndex = 0;
		}
	}

	/**
	 * Handle command execution w/ exceptions raised.
	 * 
	 * @param commandText
	 */
	private void execute(String commandText) {
		String prefixSuccess = TextFormatting.GREEN + "" + TextFormatting.BOLD + ">" + TextFormatting.RESET;
		String prefixError = TextFormatting.RED + "" + TextFormatting.BOLD + ">" + TextFormatting.RESET;
		String prefixFail = TextFormatting.RED + "" + TextFormatting.BOLD + "Fail Reason: " + TextFormatting.RESET;
		String prefixDetail = TextFormatting.RED + "" + TextFormatting.BOLD + "Details: " + TextFormatting.RESET;
		try {
			print(commandText);
			printIndexLock = true;
			Tyrant.instance().commands.executeCommand(commandText);
			updateLastSent(prefixSuccess);
		} catch (CommandNotFound e) {
			TLog.warn(e);
			updateLastSent(prefixError);
			print(prefixFail + "Command not found: " + e.getCommand());
		} catch (CommandExecutionException e) {
			TLog.warn(e);
			updateLastSent(prefixError);
			print(prefixFail + "Command execution failed: " + e.getMessage());
			print(prefixDetail + e.getMoreDetails());
		} catch (CommandPopulationException e) {
			TLog.warn(e);
			updateLastSent(prefixError);
			print(prefixFail + "Command population failed: " + e.getMessage());
			String msg = e.getMoreDetails();
			String prefixtoRemove = "Fail reason: ";
			if (msg.startsWith(prefixtoRemove)) {
				msg = msg.substring(prefixtoRemove.length());
			}
			print(prefixDetail + msg);
		}
		printIndexLock = false;
	}

	/**
	 * Updates the last unlocked messages with the given prefix. <br>
	 * See {@link #lockedMessagCount}, {@link #printIndexLock}, and
	 * {@link #unlockedCount} for more details.
	 * 
	 * @param prefix
	 */
	private void updateLastSent(String prefix) {
		try {
			// Iterate messages last sent by the console class and apply prefix
			for (int i = lockedMessagCount; i < lockedMessagCount + unlockedCount; i++) {
				String original = messages.get(i).getFormattedText();
				messages.set(i, new TextComponentString(prefix + original));
			}
			// Reset number of locked messages.
			lockedMessagCount = 0;
			printIndexLock = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void print(String text) {
		List<ITextComponent> splitContent = GuiUtilRenderComponents.splitText(new TextComponentString(text),
				this.width - 10, this.mc.fontRendererObj, false, false);
		unlockedCount = splitContent.size();
		for (ITextComponent textComponent : splitContent) {
			messages.add(0, textComponent);
			// Update number of locked messages
			// (messages being printed not by the console class)
			if (printIndexLock) {
				lockedMessagCount++;
			}
		}
	}

	/**
	 * Tab completer for commands.
	 */
	class ConsoleTabCompleter extends TabCompleter {
		public ConsoleTabCompleter(GuiTextField inputField) {
			super(inputField, false);
		}

		@Override
		protected void requestCompletions(String currentText) {
			if (currentText.length() >= 1) {
				this.requestedCompletions = true;
				String[] split = currentText.split(" ");
				String commandNamePrefix = split[0];
				// Completion of command names
				if (split.length == 1 && !currentText.endsWith(" ")) {
					List<String> commands = getCommandsStartingWith(commandNamePrefix);
					if (!commands.isEmpty()) {
						setCompletions(commands);
					}
				} else {
					Command command = Tyrant.instance().commands.getCommandMap().get(commandNamePrefix);
					if (command != null) {
						String lastSplit = split[split.length - 1];
						if (lastSplit.equals("-")) {
							setCompletions(getOptions(command, lastSplit));
						} else {
							try {
								String lastElement = (lastSplit.endsWith(commandNamePrefix)
										|| currentText.endsWith(" ")) ? "" : lastSplit;
								// TODO: Verify this works for all cases
								String[] args = Arrays.copyOfRange(split, 1, split.length);
								int paramIndex = 0;
								for (String s : args) {
									// decrement param index for optional flags
									if (!s.startsWith("-")) {
										paramIndex++;
									}
								}
								if (!currentText.endsWith(" ")) {
									paramIndex--;
								}
								// Allow commands to offer custom completions.
								setCompletions(command.getCompletions(paramIndex, args, lastElement));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}

		/**
		 * Proxy for 'setCompletions(String...)'
		 * 
		 * @param collection
		 *            Collection of strings for tab-completion.
		 */
		private void setCompletions(Collection<String> collection) {
			super.setCompletions(collection.toArray(new String[collection.size()]));
		}

		/**
		 * Return list of options for the given command starting with the given text.
		 * 
		 * @param command
		 * @param currentText
		 *            Prefix to check.
		 * @return
		 */
		private List<String> getOptions(Command command, String currentText) {
			List<String> options = new ArrayList<>();
			for (Field field : command.getClass().getDeclaredFields()) {
				// Check for option annotation
				if (field.isAnnotationPresent(Option.class)) {
					Option option = field.getAnnotation(Option.class);
					for (String name : option.names()) {
						if (name.startsWith(currentText)) {
							options.add(name);
						}
					}
				}
			}
			return options;
		}

		/**
		 * Return list of parameter fields for the given command.
		 * 
		 * @param command
		 * @return
		 */
		private List<Field> getParameterFields(Command command) {
			List<Field> params = new ArrayList<>();
			for (Field field : command.getClass().getDeclaredFields()) {
				// Check for option annotation
				if (field.isAnnotationPresent(Parameters.class)) {
					params.add(field);
				}
			}
			return params;
		}

		private List<String> getCommandsStartingWith(String prefix) {
			List<String> l = new ArrayList<>();
			for (String key : Tyrant.instance().commands.getCommandMap().keySet()) {
				if (key.startsWith(prefix)) {
					l.add(key);
				}
			}
			return l;
		}

		@Override
		public BlockPos getTargetBlockPos() {
			return null;
		}
	}
}
