package me.lpk.tyrant.gui.event;

import java.util.HashSet;
import java.util.Set;

import me.lpk.tyrant.gui.component.Component;
import me.lpk.tyrant.gui.component.type.Focusable;

/**
 * Really shit / small "event" system. Used internally by the gui api.
 * 
 * @author GenericSkid
 */
public class Events {
	private static Set<Component> components = new HashSet<>();

	public static void register(Component component) {
		components.add(component);
	}

	@SuppressWarnings("unlikely-arg-type")
	public static void focus(Focusable focused) {
		for (Component component : components) {
			if (!component.equals(focused) && component instanceof Focusable) {
				((Focusable) component).onLoseFocus();
			}
		}
		focused.onGainFocus();
	}
}
