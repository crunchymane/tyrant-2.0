package me.lpk.tyrant.gui.component.impl;

import me.lpk.tyrant.gui.component.ClickComponent;
import me.lpk.tyrant.plugin.KeyTogglePlugin;

public class PluginToggleButton extends ClickComponent {
	private final KeyTogglePlugin plugin;

	public PluginToggleButton(KeyTogglePlugin plugin) {
		super(plugin.getName());
		this.plugin = plugin;
		setClickAction(new ClickAction() {
			@Override
			public void onClick(ClickComponent caller, int mouseX, int mouseY, int button) {
				plugin.toggle();
			}
		});
	}
	
	public KeyTogglePlugin getPlugin() {
		return plugin;
	}
}
