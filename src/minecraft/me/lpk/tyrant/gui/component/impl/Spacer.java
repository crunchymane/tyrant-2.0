package me.lpk.tyrant.gui.component.impl;

import me.lpk.tyrant.gui.UIScreen;
import me.lpk.tyrant.gui.component.Component;
import me.lpk.tyrant.gui.component.Size;
import me.lpk.tyrant.gui.theme.Theme;

public class Spacer extends Component {

	public Spacer(int x, int y, int width, int height) {
		super(x, y, width, height);
		setPadding(0);
		setMargin(0);
	}
	
	@Override
	public void onRender(UIScreen screen, Theme theme) {}

	@Override
	public Size getMinimumSize() {
		return new Size(width, height);
	}

}
