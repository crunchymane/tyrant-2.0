package me.lpk.tyrant.gui.component;

import net.minecraft.client.Minecraft;

/**
 * Basic text component.
 */
public class TextComponent extends Component {
	private final String text;

	public TextComponent(String text) {
		super(0,0,getStringWidth(text), 10);
		this.text = text;
		this.setPadding(2);
		this.setOpaqueClick(false);
		this.setSize(getMinimumSize());
	}

	@Override
	public Size getMinimumSize() {
		// Cleaner way to get fontrender instance would be nice.
		return new Size(getStringWidth(text) + (getPadding(Direction.WEST) + getPadding(Direction.EAST)), Minecraft.getMinecraft().fontRendererObj.FONT_HEIGHT);
	}

	public String getText() {
		return text;
	}
	
	@Override
	public String toString() {
		return text;
	}
	
	private static int getStringWidth(String text) {
		return Minecraft.getMinecraft().fontRendererObj.getStringWidth(text)+2;
	}
}
