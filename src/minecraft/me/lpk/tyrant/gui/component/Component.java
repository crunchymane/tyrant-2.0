package me.lpk.tyrant.gui.component;

import java.util.HashMap;
import java.util.Map;

import me.lpk.tyrant.gui.UIScreen;
import me.lpk.tyrant.gui.component.type.Clickable;
import me.lpk.tyrant.gui.component.type.Hoverable;
import me.lpk.tyrant.gui.component.type.LayoutComponent;
import me.lpk.tyrant.gui.component.type.Renderable;
import me.lpk.tyrant.gui.event.Events;
import me.lpk.tyrant.gui.theme.Theme;

public abstract class Component implements Hoverable, Clickable, LayoutComponent, Renderable {
	public static final int DEFAULT_PADDING = 2;
	public static final int DEFAULT_MARGIN = 0;

	protected Map<String, Object> props = new HashMap<>();
	protected Container parent;
	protected int x, y;
	protected int width, height;

	public Component(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		Events.register(this);
	}

	@Override
	public boolean isMouseOver(int mouseX, int mouseY) {
		// Return false if clicks are to pass through the component.
		if (!getOpaqueClick()) {
			return false;
		}
		// Check for position collision.
		if (mouseX >= getDisplayX() && mouseX <= getDisplayX() + getDisplayWidth()) {
			if (mouseY >= getDisplayY() && mouseY <= getDisplayY() + getDisplayHeight()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void onRender(UIScreen screen, Theme theme) {
		theme.onRender(screen, this);
	}

	@Override
	public void onClick(int mouseX, int mouseY, int button) {}

	@Override
	public void onRelease(int mouseX, int mouseY, int button) {}

	/**
	 * Property: Opaque Click<br>
	 * If true, clicks hit the component. If false, clicks pass through the
	 * component.
	 * 
	 * @param opaque
	 *            Status to set.
	 */
	public void setOpaqueClick(boolean opaque) {
		setProperty("opaque_click", opaque ? 1 : 0);
	}

	/**
	 * Property: Opaque Click<br>
	 * If true, clicks hit the component. If false, clicks pass through the
	 * component.
	 * 
	 * @return
	 */
	public boolean getOpaqueClick() {
		return getPropertyOrDefault("opaque_click", 1).intValue() == 1;
	}

	/**
	 * Property: Padding<br>
	 * The space between the component content and the component's borders.
	 * 
	 * @return
	 */
	public int getPadding() {
		return getPropertyOrDefault("padding:" + Direction.NORTH, DEFAULT_PADDING).intValue();
	}

	/**
	 * Property: Padding:Direction<br>
	 * The space between the component content and the component's borders.
	 * 
	 * @return
	 */
	public int getPadding(Direction dir) {
		return getPropertyOrDefault("padding:" + dir.name(), DEFAULT_PADDING).intValue();
	}

	/**
	 * Property: Padding<br>
	 * The space between the component content and the component's borders.
	 * 
	 * @param value
	 *            Value to set.
	 */
	public void setPadding(int value) {
		setPadding(Direction.NORTH, value);
		setPadding(Direction.SOUTH, value);
		setPadding(Direction.EAST, value);
		setPadding(Direction.WEST, value);
	}

	/**
	 * Property: Padding:Direction<br>
	 * The space between the component content and the component's borders.
	 * 
	 * @param padding
	 *            Value to set.
	 */
	public void setPadding(Direction dir, int value) {
		setProperty("padding:" + dir.name(), value);
	}

	/**
	 * Property: Margin<br>
	 * The space between the component and the border of its parent.
	 * 
	 * @return
	 */
	public int getMargin() {
		return getPropertyOrDefault("margin:" + Direction.NORTH, DEFAULT_MARGIN).intValue();
	}

	/**
	 * Property: Margin:Direction<br>
	 * The space between the component and the border of its parent.
	 * 
	 * @return
	 */
	public int getMargin(Direction dir) {
		return getPropertyOrDefault("margin:" + dir.name(), DEFAULT_MARGIN).intValue();
	}

	/**
	 * Property: Margin<br>
	 * The space between the component and the border of its parent.
	 * 
	 * @param value
	 *            Value to set.
	 */
	public void setMargin(int value) {
		setMargin(Direction.NORTH, value);
		setMargin(Direction.SOUTH, value);
		setMargin(Direction.EAST, value);
		setMargin(Direction.WEST, value);
	}

	/**
	 * Property: Margin:Direction<br>
	 * The space between the component and the border of its parent.
	 * 
	 * @param padding
	 *            Value to set.
	 */
	public void setMargin(Direction dir, int value) {
		setProperty("margin:" + dir.name(), value);
	}

	/**
	 * Size of the element (width/height).
	 * 
	 * @return
	 */
	public Size getSize() {
		return new Size(getWidth(), getHeight());
	}

	/**
	 * Sets the value of the given property.
	 * 
	 * @param key
	 *            Property key.
	 * @param value
	 *            Value to set.
	 */
	public <T> void setProperty(String key, T value) {
		props.put(key, value);
	}

	/**
	 * Retrieves the value of the given property.
	 * 
	 * @param key
	 *            Property key.
	 * @return Value associated with key. {@code null} if no association.
	 */
	public <T> T getProperty(String key) {
		return getPropertyOrDefault(key, null);
	}
	
	/**
	 * Retrieves the value of the given property. If the key has no associated value, returns the given default value.
	 * 
	 * @param key
	 *            Property key.
	 * @return Value associated with key. {@code null} if no association.
	 */
	public <T> T getPropertyOrDefault(String key, T value) {
		return (T) props.getOrDefault(key, value);
	}

	/**
	 * Sets the size of the element. See {@link #getSize()} for more information.
	 * 
	 * @param size
	 */
	public void setSize(Size size) {
		this.setWidth(size.width);
		this.setHeight(size.height);
	}

	/**
	 * Sets the size of the element. See {@link #getSize()} for more information.
	 * 
	 * @param width
	 * @param height
	 */
	public void setSize(int width, int height) {
		this.setWidth(width);
		this.setHeight(height);
	}

	/**
	 * Returns the x-position of the component, relative to the parent.
	 * 
	 * @return
	 */
	public int getX() {
		return x;
	}

	/**
	 * Returns the y-position of the component, relative to the parent.
	 * 
	 * @return
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the x-position of the component, relative to the parent.
	 * 
	 * @param x
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Sets the y-position of the component, relative to the parent.
	 * 
	 * @param y
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Returns the x-position of the component, relative to the screen.
	 * 
	 * @return
	 */
	public int getDisplayX() {
		return parent == null ? getX() : parent.getDisplayX() + getX();
	}

	/**
	 * Returns the y-position of the component, relative to the screen.
	 * 
	 * @return
	 */
	public int getDisplayY() {
		return parent == null ? getY() : parent.getDisplayY() + getY();
	}

	/**
	 * Returns the width of the component. Does not include padding.
	 * 
	 * @return
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Returns the height of the component.
	 * 
	 * @return
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Returns the width of the component. Includes padding.
	 * 
	 * @return
	 */
	public int getDisplayWidth() {
		return getWidth() + getPadding(Direction.EAST) + getPadding(Direction.WEST);
	}

	/**
	 * Returns the height of the component. Includes padding.
	 * 
	 * @return
	 */
	public int getDisplayHeight() {
		return getHeight() + getPadding(Direction.NORTH) + getPadding(Direction.SOUTH);
	}

	/**
	 * Sets the width of the component.
	 * 
	 * @param width
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Sets the height of the component.
	 * 
	 * @param height
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Returns the container component that holds this component.
	 * 
	 * @return
	 */
	public Container getParent() {
		return parent;
	}

	/**
	 * Sets the parent container for the component.
	 * 
	 * @param parent
	 *            Component to hold this.
	 */
	public void setParent(Container parent) {
		this.parent = parent;
	}

	public static enum Direction {
		NORTH, EAST, SOUTH, WEST, CENTER;
	}
}
