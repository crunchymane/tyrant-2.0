package me.lpk.tyrant.gui.component;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import me.lpk.tyrant.gui.UIScreen;
import me.lpk.tyrant.gui.component.type.Layout;
import me.lpk.tyrant.gui.component.type.LayoutContainer;
import me.lpk.tyrant.gui.theme.Theme;

public class Container extends Component implements LayoutContainer {
	private final Set<Component> children = new LinkedHashSet<>();
	private Layout layout;

	public Container(int x, int y, int width, int height) {
		super(x, y, width, height);
		this.setClickThroughChildren(false);
	}

	@Override
	public void onRender(UIScreen screen, Theme theme) {
		super.onRender(screen, theme);
		for (Component child : getChildren()) {
			child.onRender(screen, theme);
		}
	}

	@Override
	public Layout getLayout() {
		return layout;
	}

	@Override
	public void setLayout(Layout layout) {
		this.layout = layout;
	}

	@Override
	public boolean isMouseOver(int mouseX, int mouseY) {
		// Children (like a button) should prevent the container from being
		// hovered since they are between the mouse and the container.
		if (!getClickThroughChildren()) {
			for (Component child : getChildren()) {
				if (child.isMouseOver(mouseX, mouseY)) {
					return false;
				}
			}
		}
		return super.isMouseOver(mouseX, mouseY);
	}

	@Override
	public Container getContainer() {
		return this;
	}

	@Override
	public Collection<Component> getChildren() {
		return children;
	}

	@Override
	public Size getMinimumSize() {
		// By default the minimum size of a container is the current size.
		// Containers shapes by default are not to be dependant on their
		// children's sizes.
		return new Size(getWidth(), getHeight());
	}

	/**
	 * Property: Click Through Children<br>
	 * If true, clicks hit chilren component and do not click the container. If
	 * false, clicks pass through the the chilren onto the container.
	 * 
	 * @param opaque
	 *            Status to set.
	 */
	public boolean getClickThroughChildren() {
		return this.getPropertyOrDefault("transparent_children", true);
	}

	/**
	 * Property: Click Through Children<br>
	 * If true, clicks hit chilren component and do not click the container. If
	 * false, clicks pass through the the chilren onto the container.
	 * 
	 * @param value
	 * 
	 * @return
	 */
	public void setClickThroughChildren(boolean value) {
		this.setProperty("transparent_children", value);
	}

	/**
	 * Reduce the width and height of the container to fit the exact space needed to
	 * show all the children components. If no children exist, nothing will happen.
	 */
	public void pack() {
		if (getChildren().size() > 0) {
			Size packedSize = getPackedSize();
			setWidth(packedSize.width);
			setHeight(packedSize.height);
		}
	}

	/**
	 * Finds the minimum size needed to contain the children components.
	 * 
	 * @return
	 */
	protected Size getPackedSize() {
		return getLayout().getPackedSize(this);
	}
}
