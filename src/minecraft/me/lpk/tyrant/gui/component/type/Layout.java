package me.lpk.tyrant.gui.component.type;

import me.lpk.tyrant.gui.component.Size;

public interface Layout {
	Size getPackedSize(LayoutContainer owner);

	void invalidate();
}