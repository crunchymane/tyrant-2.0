package me.lpk.tyrant.gui.component.type;

public interface Clickable {
	void onClick(int mouseX, int mouseY, int button);
	
	void onRelease(int mouseX, int mouseY, int button);
}
