import java.util.Arrays;

import me.lpk.tyrant.Tyrant;
import net.minecraft.client.main.Main;

public class Start {
	public static int testing = 0;

	public static void main(String[] args) {
		if (testing == 1) {
			System.out.println("======================== CALL <INIT> =========================");
			Tyrant t = new Tyrant();
			System.out.println("======================== CALL RELOAD =========================");
			t.reload();
			System.out.println("======================== CALL UNLOAD =========================");
			t.unload();
			System.exit(0);
		}
		try {
			Main.main(concat(new String[] { "--username", "Tyrant", "--version", "1.12", "--accessToken", "0",
					"--assetsDir", "assets", "--assetIndex", "1.12", "--userProperties", "{}" }, args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static <T> T[] concat(T[] first, T[] second) {
		T[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}
}
