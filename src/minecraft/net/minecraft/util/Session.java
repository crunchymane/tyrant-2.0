package net.minecraft.util;

import com.google.common.collect.Maps;
import com.mojang.authlib.Agent;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.InvalidCredentialsException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;
import com.mojang.util.UUIDTypeAdapter;

import me.lpk.tyrant.util.TLog;

import java.net.Proxy;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javax.annotation.Nullable;

public class Session {
	private final String username;
	private final String playerID;
	private final String token;
	private final Session.Type sessionType;

	public Session(String usernameIn, String playerIDIn, String tokenIn, String sessionTypeIn) {
		this.username = usernameIn;
		this.playerID = playerIDIn;
		this.token = tokenIn;
		this.sessionType = Session.Type.setSessionType(sessionTypeIn);
	}

	public String getSessionID() {
		return "token:" + this.token + ":" + this.playerID;
	}

	public String getPlayerID() {
		return this.playerID;
	}

	public String getUsername() {
		return this.username;
	}

	public String getToken() {
		return this.token;
	}

	public GameProfile getProfile() {
		try {
			UUID uuid = UUIDTypeAdapter.fromString(this.getPlayerID());
			return new GameProfile(uuid, this.getUsername());
		} catch (IllegalArgumentException var2) {
			return new GameProfile((UUID) null, this.getUsername());
		}
	}

	public static enum Type {
		LEGACY("legacy"), MOJANG("mojang");

		private static final Map<String, Session.Type> SESSION_TYPES = Maps.<String, Session.Type> newHashMap();
		private final String sessionType;

		private Type(String sessionTypeIn) {
			this.sessionType = sessionTypeIn;
		}

		@Nullable
		public static Session.Type setSessionType(String sessionTypeIn) {
			return SESSION_TYPES.get(sessionTypeIn.toLowerCase(Locale.ROOT));
		}

		static {
			for (Session.Type session$type : values()) {
				SESSION_TYPES.put(session$type.sessionType, session$type);
			}
		}
	}

	public static Session login(String userInput) {
		return new Session(userInput, UUID.randomUUID().toString(), UUID.randomUUID().toString(), "legacy");
	}
	
	public static Session login(String userInput, String password) {
		if (userInput == null || userInput.length() <= 0 || password == null || password.length() <= 0) {
			return null;
		}
		YggdrasilAuthenticationService sessService = new YggdrasilAuthenticationService(Proxy.NO_PROXY, "");
		YggdrasilUserAuthentication userAuth = (YggdrasilUserAuthentication) sessService
				.createUserAuthentication(Agent.MINECRAFT);
		userAuth.setUsername(userInput);
		userAuth.setPassword(password);
		if (userAuth.canLogIn()) {
			try {
				userAuth.logIn();
				String username = userAuth.getSelectedProfile().getName();
				String playerID = userAuth.getSelectedProfile().getId().toString();
				String token = userAuth.getAuthenticatedToken();
				String sessionType = userInput.contains("@") ? "MOJANG" : "LEGACY";
				return new Session(username, playerID, token, sessionType);
			} catch (InvalidCredentialsException e) {
				TLog.info("Invalid credentials for: " + userInput);
			} catch (AuthenticationException e) {
				TLog.info("Authentication error for: " + userInput);
			}
		}
		return null;
	}
}
