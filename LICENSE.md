# [Tyrant (2.0)](https://bitbucket.org/lordpankake/tyrant-2.0)

Tyrant is (c) Copyright by [Lord Pankake](https://bitbucket.org/lordpankake/)

## Terms of use.

### You are allowed to:

* Modify and redistribute the source code assuming the following conditions apply:
    * Copyright is kept in redistributed files in the top-most directory
    * Copyright is not modified
    * A history or list of the changes from the source material are attached.
* Create content-packs *(plugins, configurations, language-packs, etc.)* for Tyrant
    * Packs may be used in commercial settings assuming the following conditions apply:
        * The pack does not contain Tyrant in source or compiled form. References to public API's are not included.
        * The pack does not contain malware, links to malware, or anything that can cause intentional harm to a user and their property.

### You are NOT allowed to:

* Use Tyrant in commercial settings unless explicit permission is given
    * This excludes content-packs as described in the section titled *"You are allowed to"*
    * This excludes text and video reviews where revenue is generated soley from advertising on the page *(Such as youtube or blogs with google-adsense)*
* Hold Tyrant for any damages to your system. You will use Tyrant at your own risk. You are responsible for ensuring the security of your system. 