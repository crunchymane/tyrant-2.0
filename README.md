# [Tyrant (2.0)](https://bitbucket.org/lordpankake/tyrant-2.0)

Tyrant is a client mod for Minecraft. 

### Download

Official downloads can be seen in the [*"Downloads"*](https://bitbucket.org/lordpankake/tyrant-2.0/downloads/) tab.

### Pre-Requisites

In order to run Tyrant **you need to be running the game through the JDK,not the JRE**. You can download the JDK [here](http://www.oracle.com/technetwork/java/javase/downloads/index.html). Installing the JDK will update any existing java versions you have installed on your machine. Minecraft will use the current java version installed on your machine *(Or a bundled version if none exists)*. By default it will **not choose the JDK**. In your profile ensure that your executable option path looks like the following:

```
C:\Program Files\Java\jdk1.8.0_131\jre\bin\javaw.exe
```

### Contact

* [Hackforums](https://hackforums.net/member.php?action=profile&uid=1828119)
* [Discord](https://discord.gg/t6BQqpm) - Tyrant Discord

### Copyright / Terms of use

* [LICENSE.md](https://bitbucket.org/lordpankake/tyrant-2.0/src/master/LICENSE.md)